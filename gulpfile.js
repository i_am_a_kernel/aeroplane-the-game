var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var rename = require("gulp-rename");
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');
var connect = require('gulp-connect');
var imagemin = require('gulp-imagemin');
var less = require('gulp-less');
var clean = require('gulp-clean');

// Основные
gulp.task('clean', function () {
    return gulp.src('./dist', {read: false})
        .pipe(clean());
});

gulp.task('css',function () {
  gulp.src('./app/css/**/*.less')
    .pipe(less())
    .pipe(concatCss("style.min.css"))
    //.pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('./dist/css/'));
});


gulp.task('html',function(){
    gulp.src(['./app/*.html','./app/favicon.ico'])
        .pipe(gulp.dest('./dist/'))
        .pipe(connect.reload());
});

gulp.task('fonts',function(){
    gulp.src('./app/font/**/*')
        .pipe(gulp.dest('./dist/font/'))
        .pipe(connect.reload());
});

gulp.task('img',function(){
    gulp.src('./app/img/**/*')
        .pipe(gulp.dest('./dist/img/'))
        .pipe(connect.reload());
});

gulp.task('js',function(){
    gulp.src('./app/js/*.js')
    //.pipe(uglify())
        .pipe(gulp.dest('./dist/js/'))
        .pipe(connect.reload());
    
    gulp.src('./app/js/static/*.js')
    //.pipe(uglify())
        .pipe(gulp.dest('./dist/js/static'))
        .pipe(connect.reload());
});

gulp.task('jslibs',function(){
    gulp.src('./app/js/vendor/**/*.js')
        .pipe(gulp.dest('./dist/js/vendor/'))
        .pipe(connect.reload());
});

gulp.task('jsmods',function(){
    gulp.src('./app/js/src/**/*.js')
    //.pipe(uglify())
        .pipe(gulp.dest('./dist/js/src/'))
        .pipe(connect.reload());
});

gulp.task('3d',function(){
    gulp.src(['./app/js/models/**/*.json','./app/js/models/**/*.jpg'])
        .pipe(gulp.dest('./dist/js/models/'))
        .pipe(connect.reload());
});

gulp.task('shader',function(){
    gulp.src('./app/js/shaders/**/*.glsl')
        .pipe(gulp.dest('./dist/js/shaders/'))
        .pipe(connect.reload());
});

gulp.task('templates',function(){
    gulp.src('./app/js/templates/**/*.tmp')
        .pipe(gulp.dest('./dist/js/templates/'))
        .pipe(connect.reload());
});

// Connect
gulp.task('connect', function() {
  connect.server({
    root: 'dist',
    livereload: true
  });
});

// Watch
gulp.task('watch',function(){
    gulp.watch("./app/css/**/*.less", ["css"]);
    gulp.watch("./app/*.html", ["html"]);
    gulp.watch("./app/js/*.js", ["js"]);
    gulp.watch('./app/js/static/*.js', ["js"]);
    gulp.watch("./app/js/vendor/**/*.js", ["jslibs"]);
    gulp.watch("./app/js/src/**/*.js", ["jsmods"]);
    gulp.watch("./app/js/shaders/**/*.glsl", ["shader"]);
    gulp.watch("./app/js/templates/**/*.tmp", ["templates"]);    
});

gulp.task('build',["html", "css", "js","jslibs", "jsmods", "3d", "shader", "templates", "img"])

gulp.task('dev', ["build", "connect", "watch"]);

// Default
gulp.task('default', ["dev"]);

