require(['src/utils/detector', 'src/app', 'src/utils/container', 'src/configuration', 'src/utils/notificator'],
    function (detector, app, container, config, notificator) {
        if (!detector.webgl) {
            var error = detector.getWebGLErrorMessage();
            if (error) {
                notificator.showError(error.title, error.msg, error.actionLink, error.actionName);
            } else {
                notificator.showError('Compatibility problems', 'The game can not be started in this browser. Try using a another one or install a newer version.');
            }
        }

        var browserCompatibilityFail = detector.browser;
        if (browserCompatibilityFail) {
            notificator.showError(browserCompatibilityFail.title, browserCompatibilityFail.msg);

            console.error('Game loading aborted. Browser compatibility problems');
            return;
        }


        setTimeout(function () {
            //Initialize game app and start animation loop
            app.init().then(function () {
                console.log('GAME STARTED...')
                var hidedElements = document.querySelectorAll("*[hidden-hud]");
                hidedElements.forEach(function (element) {
                    element.removeAttribute("hidden-hud");
                });

                var loadingElements = document.querySelectorAll(".header [loading]");
                loadingElements.forEach(function (element) {
                    element.removeAttribute("loading");
                });

                document.querySelector("*[ver-indicator]").removeAttribute("ver-indicator");

                app.loop();
            }, function () {
                console.error("Loading failed. Try to reload page");
                notificator.showError('Loading failed', 'Check you internet connection and reload page.');
            });
        }, 4000);
    })