uniform vec3 bottomColor;
uniform vec3 middleColor;
uniform vec3 topColor;
uniform vec3 seaColor;
uniform float bottomAlt;
uniform float topAlt;
uniform float seaAlt;

varying vec3 vPosition;
varying float distFromCenter;
varying vec3 vViewPosition;

// chunk(common);
// chunk(packing);
// chunk(fog_pars_fragment);
// chunk(lights_pars);
// chunk(shadowmap_pars_fragment);
// chunk(shadowmask_pars_fragment);

vec3 colorNormal(vec3 normal, vec3 col){
    return clamp(col * normal.x + col * normal.y + col * normal.z, vec3(0.0), vec3(1.0));
}

void main() {    
	vec3 fdx = vec3( dFdx( vViewPosition.x ), dFdx( vViewPosition.y ), dFdx( vViewPosition.z ) );
	vec3 fdy = vec3( dFdy( vViewPosition.x ), dFdy( vViewPosition.y ), dFdy( vViewPosition.z ) );
	vec3 normal = normalize( cross( fdx, fdy ) );

    // chunk(lights_template);

    float opacity = 1.0;
    float p = ( 1.0  - ( 1.0 - getShadowMask())) - 0.1;
    //gl_FragColor = vec4(normal * bottomColor.xyz * p, opacity );

    if(distFromCenter < seaAlt)
    {
        gl_FragColor = vec4(colorNormal(normal, seaColor.xyz  * p), opacity );
    }
    if(distFromCenter <= bottomAlt && distFromCenter >= seaAlt)
    {
        gl_FragColor = vec4(colorNormal(normal, bottomColor.xyz  * p), opacity );
    }
    if(distFromCenter > bottomAlt && distFromCenter < topAlt)
    {
        gl_FragColor = vec4(colorNormal(normal, middleColor.xyz  * p), opacity );
    }
    if(distFromCenter >= topAlt)
    {
        gl_FragColor = vec4(colorNormal(normal, topColor.xyz  * p), opacity );
    }

    // chunk(fog_fragment);
    // chunk(dithering_fragment);

    // vec4 rawColor;
    // if(distFromCenter >= 580.0 && distFromCenter < 605.0){
    //     rawColor = vec4(colorNormal(fNormal, vec3(1.0, 0.0, 0.0), vec3(1.0, 0.0, 0.0), vec3(1.0, 0.0, 0.0)), 1.0);
    // }else if(distFromCenter >= 605.0 && distFromCenter < 620.0){
    //     rawColor = vec4(colorNormal(fNormal, vec3(0.0, 1.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 1.0, 0.0)), 1.0);
    // }else{
    //     rawColor = vec4(colorNormal(fNormal, vec3(0.0, 0.0, 1.0), vec3(0.0, 0.0, 1.0), vec3(0.0, 0.0, 1.0)), 1.0);
    // }
} 