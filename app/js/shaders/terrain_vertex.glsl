varying vec3 vPosition;
varying float distFromCenter;
varying vec3 vViewPosition;

// chunk(common);
// chunk(fog_pars_vertex);
// chunk(shadowmap_pars_vertex);

void main() 
{
    distFromCenter = length(vec4(position, 1.0).xy);

    vec3 objectNormal = vec3(normal);
    vec3 transformedNormal = normalMatrix * objectNormal;
#ifdef FLIP_SIDED
	transformedNormal = -transformedNormal;
#endif

    vec3 transformed = vec3(position);
    vec4 mvPosition = modelViewMatrix * vec4(transformed, 1.0);
    vViewPosition = -mvPosition.xyz;
    
    vec4 worldPosition = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * mvPosition;

    // chunk(shadowmap_vertex);
    // chunk(fog_vertex);
}