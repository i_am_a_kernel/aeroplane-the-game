define([], function () {
    'use strict';

    var conf = {
        COLORS: {
            green: 0x819603,
            brown: 0x8b7346,
            white: 0xf4efe5,
            sky: 0xccdfef,
            navy: 0x397cb6,
            pink: 0xF5986E,
            red: 0xf25346,
            brownDark: 0x23190f
        },
        COUNT_OF_CLOUDS: 45,
        LOADING_STATE: [
            'Compatibility check...',
            'Creating a scene...',
            'Loading models...',
            'Creating lighting...',
            'Assemble the aeroplane...',
            'Obtaining a pilot\'s license...',
            'Finishing touches...'
        ],
        GAME_STATE: {
            gaming: 0,
            gameover: 1,
            waitingReplay: 2,
            pause: 3
        },
        MODAL_TYPES: {
            market: 0,
            hangar: 1,
            news: 2
        },
        VER: '1.1.0',
        MARKET_ITEMS: [
            {
                id: 101,
                name: 'Paper plane',
                path: 'js/models/plane_1/plane_1.json',
                icon: 'img/plane_1-icon.jpg',
                scaleX: 2.5,
                scaleY: 2.5,
                scaleZ: 2.5,
                mobileScaleX: 1.5,
                mobileScaleY: 1.5,
                mobileScaleZ: 1.5,
                price: 0,
                maxSpeed: 0.3,
                fuelConsamption: 0.2,
                control: 0.35,
                strength: 0.1,
                available: true
            },
            {
                id: 102,
                name: 'Biplane',
                path: 'js/models/plane_2/plane_2.json',
                icon: 'img/plane_2-icon.jpg',
                scaleX: 1,
                scaleY: 1,
                scaleZ: 1,
                mobileScaleX: 0.7,
                mobileScaleY: 0.7,
                mobileScaleZ: 0.7,
                price: 31,
                maxSpeed: 0.4,
                fuelConsamption: 0.3,
                control: 0.3,
                strength: 0.3,
                available: true
            },
            {
                id: 103,
                name: 'Ford 5-AT Tri-Motor',
                path: 'js/models/plane_3/plane_3.json',
                icon: 'img/plane_3-icon.jpg',
                scaleX: 1.25,
                scaleY: 1.25,
                scaleZ: 1.25,
                mobileScaleX: 0.9,
                mobileScaleY: 0.9,
                mobileScaleZ: 0.9,
                price: 61,
                maxSpeed: 0.5,
                fuelConsamption: 0.3,
                control: 0.25,
                strength: 0.4,
                available: true
            },
            {
                id: 104,
                name: 'LZ 127 Graf Zeppelin',
                path: '',
                icon: 'img/holder.jpg',
                scaleX: 1.25,
                scaleY: 1.25,
                scaleZ: 1.25,
                mobileScaleX: 0.9,
                mobileScaleY: 0.9,
                mobileScaleZ: 0.9,
                price: 61,
                maxSpeed: 0.5,
                fuelConsamption: 0.3,
                control: 0.25,
                strength: 0.4,
                available: false
            },
            {
                id: 105,
                name: 'Canadair CL-215 (firefighter service)',
                path: '',
                icon: 'img/holder.jpg',
                scaleX: 1.25,
                scaleY: 1.25,
                scaleZ: 1.25,
                mobileScaleX: 0.9,
                mobileScaleY: 0.9,
                mobileScaleZ: 0.9,
                price: 61,
                maxSpeed: 0.5,
                fuelConsamption: 0.3,
                control: 0.25,
                strength: 0.4,
                available: false
            },
            {
                id: 106,
                name: 'F4U Corsair',
                path: '',
                icon: 'img/holder.jpg',
                scaleX: 1.25,
                scaleY: 1.25,
                scaleZ: 1.25,
                mobileScaleX: 0.9,
                mobileScaleY: 0.9,
                mobileScaleZ: 0.9,
                price: 61,
                maxSpeed: 0.5,
                fuelConsamption: 0.3,
                control: 0.25,
                strength: 0.4,
                available: false
            }
        ]
    }

    return conf;
});