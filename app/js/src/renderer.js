define(['three', 'src/utils/container'], function (THREE, container) {
    'use strict';

    var privateRenderer = undefined;

    var obj = {
        create: function () {
            var c = container.find();
            var renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
            renderer.setSize(c.parentNode.offsetWidth, c.parentNode.offsetHeight);
            renderer.shadowMap.enabled = true;

            while (c.firstChild) {
                c.removeChild(c.firstChild);
            }
            c.removeAttribute("loading");
            c.appendChild(renderer.domElement);

            var resizeEventHandler = function () {
                renderer.setSize(c.parentNode.offsetWidth, c.parentNode.offsetHeight);
            }

            privateRenderer = renderer;
            window.addEventListener('resize', resizeEventHandler, false);
            return renderer;
        },
        get: function(){
            if(privateRenderer) return privateRenderer;

            throw new Error('Renderer in not created yet!');
        }
    }

    return obj;
});