define(['src/utils/simple-promise'], function(SimplePromise) {
    'use strict';
    
    var SimpleDefer = function(){
        this.promise = new SimplePromise();
    }

    SimpleDefer.prototype = {
        promise: null,
        resolve: function(data){
            this.promise.okCallbacks.forEach(function(callback){
                window.setTimeout(function(){
                    callback(data);
                }, 0);
            });
        },
        reject: function(error){
            this.promise.koCallbacks.forEach(function(callback){
                window.setTimeout(function(){
                    callback(error);
                }, 0);
            })
        }
    }

    return SimpleDefer;
});