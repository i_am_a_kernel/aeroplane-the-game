/**
 * @author Arthur Osmokiesku
 */

define([
	'bowser',
], function (bowser) {
	'use strict';

	var bowser = require('bowser');

	var Detector = {
		canvas: !!window.CanvasRenderingContext2D,
		webgl: (function () { try { return !!window.WebGLRenderingContext && !!document.createElement('canvas').getContext('experimental-webgl'); } catch (e) { return false; } })(),
		workers: !!window.Worker,
		fileapi: window.File && window.FileReader && window.FileList && window.Blob,
		browser: (function () {
			if (bowser.msie) {
				//Unsuported desktop IE
				return {
					title: 'You browser is out-of-date',
					msg: 'We detect that you use unsuported browser IE. Please use another browser to play the game.'
				};
			}

			if (bowser.safari && bowser.version < 8) {
				//Unsuported desktop safari
				return {
					title: 'You browser is out-of-date',
					msg: 'We detect that you use unsuported browser Safari < 8. Please update you browser to play the game.'
				};
			}

			if (bowser.windowsphone) {
				//Unsuported WP
				return {
					title: 'Unsuported mobile platform',
					msg: 'We detect that you use Windows Phone. Unfortunately, we do not support this platform.'
				};
			}

			return undefined;
		})(),

		isMobile: function () {
			return bowser.mobile ? true : false;
		},

		isMobilePortrait: function () {
			return this.isMobile() && window.innerHeight > window.innerWidth;
		},

		getWebGLErrorMessage: function () {
			if (!this.webgl) {

				if (!window.WebGLRenderingContext) {
					return {
						title: 'Compatibility problems',
						msg: 'Your graphics card does not support WebGL.',
						actionLink: 'http://get.webgl.org',
						actionName: 'Find out how to get it here'
					}
				} else {
					return {
						title: 'Compatibility problems',
						msg: 'Your browser does not support WebGL.',
						actionLink: 'http://get.webgl.org',
						actionName: 'Find out how to get it here'
					}
				}
			}

			return undefined;
		}
	};


	return Detector;
});

