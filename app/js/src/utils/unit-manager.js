define(['src/state', 'src/configuration', 'src/utils/save-provider', 'src/utils/modal-provider'], function (state, config, saver, modals) {
    'use strict';

    return {
        buy: function(){
            var id = parseInt(this.dataset.id);

            var p = config.MARKET_ITEMS.find(function(item){
                return item.id === id;
            });

            if(!p){
                console.error('Plane with id='+id+' not found in market');
                return;
            }

            if(state.money < p.price){
                console.error('User can not purchase plane with id='+id+'. Money in wallet: '+state.money);
                return;
            }

            if(state.planes.includes(id)){
                console.error('User already has plane with id='+id+'in hangar.');
                return;
            }

            state.planes.push(id);

            saver.set({
                money: saver.get(['money']).money - p.price,
                planes: state.planes
            });

            state.money = saver.get().money;
            modals.close();
        },

        pick: function(){
            var id = parseInt(this.dataset.id);

            var p = config.MARKET_ITEMS.find(function(item){
                return item.id === id;
            });

            if(!p){
                console.error('Plane with id='+id+' not found');
                return;
            }

            if(state.selected === p.id){
                console.error('User already select plane with id='+id+'.');
                return;
            }

            if(!state.planes.includes(id)){
                console.error('Plane with id id='+id+'in hangar.');
                return;
            }

            state.selected = id;

            saver.set({
                selected: state.selected
            });

            modals.close();
        }
    }
})