define(['src/utils/container'], function(container){
    'use strict';

    function show(root, title, msg){
        if(title){
            root.querySelector('.title span').innerText = title;
        }

        if(msg){
            root.querySelector(".description").innerText = msg;
        }
        root.classList.remove('no-display');
    }

    function hide(root){
        root.classList.add('no-display');
    }

    var obj = {
        showError: function(title, msg, actionLink, actionName){
            var notificationBlock = container.find('container').querySelector('#notification');
            notificationBlock.classList.remove("ok");
            notificationBlock.classList.remove("error");
            notificationBlock.classList.add("error");

            if(actionLink && actionName){
                var act = notificationBlock.querySelector('#action');
                act.innerText = actionName;
                act.setAttribute('href', actionLink);
                notificationBlock.querySelector('#action').classList.remove('no-display');
            }else{
                notificationBlock.querySelector('#action').classList.add('no-display');
            }

            show(notificationBlock, title, msg);
        },
        showOk: function(title, msg, actionLink){
            var notificationBlock = container.find('container').querySelector('#notification');
            notificationBlock.classList.remove("error");
            notificationBlock.classList.remove("ok");
            notificationBlock.classList.add("ok");

            show(notificationBlock, title, msg);
            setTimeout(function(){
                hide(notificationBlock);
            }, 10000);
        }
    }

    return obj;
})