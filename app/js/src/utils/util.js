define(['three'], function (THREE) {
    'use strict';

    function replaceThreeChunkFn(a, b){
        var chunk =  THREE.ShaderChunk[b] + '\n';
        if(!chunk){
            console.error('Shader chunk ' + b + 'not found.');
        }
        return chunk;
    }

    var obj = {
        normalize: function (v, vmin, vmax, tmin, tmax) {
            var nv = Math.max(Math.min(v, vmax), vmin);
            var dv = vmax - vmin;
            var pc = (nv - vmin) / dv;
            var dt = tmax - tmin;
            var tv = tmin + (pc * dt);
            return tv;
        },
        findElementById: function(id){
            return document.getElementById(id);
        },
        shaderParse(glsl){
            return glsl.replace(/\/\/\s?chunk\(\s?(\w+)\s?\);/g, replaceThreeChunkFn);
        }
    }

    return obj;
})