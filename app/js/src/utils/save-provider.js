define(['src/state', 'crypto-js/hmac-sha256', 'crypto-js/enc-base64', 'src/configuration'], function (state, crypto, enc, config) {
    'use strict';
    const key = 'save';

    function produceSecret() {
        var ln = navigator.language;
        var timeOffset = new Date().getTimezoneOffset();
        var w = screen.width;
        var h = screen.height;
        var screenDpth = screen.colorDepth;
        var os = navigator.platform;
        return [ln, timeOffset, w, h, screenDpth, os].join("_");;
    }

    function sign(xxxx) {
        var s = produceSecret();
        var hashedData = crypto(xxxx, s);
        var encodedHashData = enc.stringify(hashedData);
        return encodedHashData;
    }

    function verify(xxxx, stamp) {
        return stamp === sign(xxxx);
    }

    var obj = {
        set: function (snapshot) {
            var item = this.get();

            if (snapshot) {
                for (var property in snapshot) {
                    item[property] = snapshot[property];
                }
            } else {
                item = {
                    bestScore: Math.round(state.distance),
                    maxLevel: state.level,
                    money: state.money,
                    planes: state.planes,
                    selected: state.selected,
                    lastSignInVer: config.VER
                };
            }

            var save = btoa(JSON.stringify(item));
            var signature = sign(save)
            var encodedSave = [save, signature].join('.');
            localStorage.setItem(key, encodedSave);
        },
        get: function (props) {
            var item = {
                bestScore: 0,
                maxLevel: 0,
                money: 0,
                planes: [101],
                selected: 101,
                lastSignInVer: '0'
            };
            var save = localStorage.getItem(key)
            if (save) {
                var rawSave = save.split(".");
                var isVerified = verify(rawSave[0], rawSave[1]);
                if (isVerified) {
                    var decryptedSaveData = JSON.parse(atob(rawSave[0]));
                    if (props) {
                        for (var propertyName in props) {
                            item[props[propertyName]] = decryptedSaveData[props[propertyName]];
                        }
                    } else {
                        for (var propertyName in decryptedSaveData) {
                            item[propertyName] = decryptedSaveData[propertyName];
                        }
                    }
                }
            }
            return item;
        }
    }

    return obj;
});