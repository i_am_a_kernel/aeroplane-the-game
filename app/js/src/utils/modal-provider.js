define(function (require) {
    'use strict';

    var container = require('src/utils/container');
    var config = require('src/configuration');
    var hbs = require('handlebars');

    var marketModalTemplate = require("text!templates/market.tmp");
    var hangarModalTemplate = require("text!templates/hangar.tmp");
    var newsModalTemplate = require('text!templates/news.tmp');

    var obj = {
        close: function(){
            var e = new Event("click");
            e.data = document.getElementById('modal');
            window.dispatchEvent(e);
        },
        open: function (title, type, context, onOpenCallback ,onCloseCallback) {
            if(!title || type === undefined || !context){
                console.error("Can't open modal without title, type or context.");
                return;
            }

            // Get the modal
            var modal = document.getElementById('modal');

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            var overlay = container.find("container");
            overlay.style.filter = "blur(2px)"

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
                overlay.style.filter = null;
                onCloseCallback();
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal || event.data == modal) {
                    modal.style.display = "none";
                    overlay.style.filter = null;
                    onCloseCallback();
                }
            }

            modal.querySelector('.modal-header h2').innerText = title;
            var body = modal.querySelector('.modal-body');

            var source;
            switch(type){
                case config.MODAL_TYPES.hangar:
                    source = hangarModalTemplate;
                    break;
                case config.MODAL_TYPES.market:
                    source = marketModalTemplate;
                    break;
                case config.MODAL_TYPES.news:
                    source = newsModalTemplate;
                    break;
            }
            
            var compiledTemplate = hbs.compile(source);
            var html = compiledTemplate(context);
            body.innerHTML = html;

            onOpenCallback();

            modal.style.display = "block";
        }
    }

    return obj;
})