define([], function() {
    'use strict';
    
    var SimplePromise = function(){
        this.okCallbacks = [];
        this.koCallbacks = [];
    }

    SimplePromise.prototype = {
        okCallbacks: null,
        koCallbacks: null,
        then: function(okCallback, koCallback){
            this.okCallbacks.push(okCallback);
            if(koCallback){
                this.koCallbacks.push(koCallback);
            }
        }
    }

    return SimplePromise;
});