define(['three', 'src/configuration'], function (THREE, configuration) {
    'use strict';

    var threeLoader = new THREE.JSONLoader();

    var loader = {
        load: function (modelId, primaryCallback, secondaryCallback) {

            var path = configuration.MARKET_ITEMS.filter(function (obj) {
                return obj.id === modelId;
            })[0].path

            return new Promise(function (resolve, reject) {
                threeLoader.load(path, function (geom, materials) {
                    if (!primaryCallback) {
                        var exception = new Error("primaryCallback is required for mesh loader.");
                        console.error(exception);
                        reject(exception);
                    }

                    var mesh = new THREE.Mesh(geom, new THREE.MeshFaceMaterial(materials));
                    var updatedMesh = primaryCallback(mesh, modelId);

                    if (secondaryCallback && updatedMesh) {
                        secondaryCallback(updatedMesh);
                    }
                    resolve(updatedMesh)

                }, function (xhr) {
                    console.log((xhr.loaded / xhr.total * 100) + '% loaded');
                }, function (error) {
                    var exception = new Error(error);
                    console.error(exception);
                    reject(exception);
                })
            })
        }
    };

    return loader;
})