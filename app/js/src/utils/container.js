define([], function() {
    'use strict';

    return {
        find: function(id){
            if(id){
                return document.getElementById(id);
            }

            return document.getElementById('threejs-container');
        }
    }
});