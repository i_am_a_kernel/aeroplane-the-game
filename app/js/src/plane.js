define(['three', 'src/configuration', 'src/state', 'src/particles', 'src/utils/util', 'src/utils/mesh-loader', 'src/utils/detector'], function (THREE, config, state, particle, util, loader, detector) {
    'use strict';

    var obj = {
        construct: function (baseMesh, modelId) {
            var manifest = config.MARKET_ITEMS.filter(function (obj) {
                return obj.id === modelId;
            })[0]

            baseMesh.castShadow = true;
            baseMesh.receiveShadow = true;

            var planeStats = config.MARKET_ITEMS.filter(function(item){
                return item.id === state.selected;
            })[0];

            if(!planeStats) {
                console.error("Plane stats not found. Try to reload game.")
                return;
            }

            var planer = {
                mesh: new THREE.Object3D(),
                name: 'aeroPlane',
                maxSpeed: planeStats.maxSpeed,
                fuelConsumption: planeStats.fuelConsamption,
                control: planeStats.control,
                strength: planeStats.strength
            }
            planer.mesh.add(baseMesh);

            detector.isMobile()
                ? planer.mesh.scale.set(manifest.mobileScaleX, manifest.mobileScaleY, manifest.mobileScaleZ)
                : planer.mesh.scale.set(manifest.scaleX, manifest.scaleY, manifest.scaleZ)
            planer.mesh.position.y = state.planeDefaultHeight;

            return planer;
        },
        animate: function (plane, mousePosition, deltaTime, isMobilePortrait) {
            if (typeof isMobilePortrait === "undefined") isMobilePortrait = false;

            var targetY = util.normalize(mousePosition.y, -.75, .75, state.planeDefaultHeight - state.planeDownAmpHeight, state.planeDefaultHeight + state.planeUpAmpHeight);
            var targetX = 0;
            if (isMobilePortrait) {
                targetX = util.normalize(mousePosition.x, -1, 1, -state.planeAmpWidth * .4, 0);
            } else {
                targetX = util.normalize(mousePosition.x, -1, 1, -state.planeAmpWidth * .7, -state.planeAmpWidth);
            }

            targetX += state.planeCollisionDisplacementX;
            targetY += state.planeCollisionDisplacementY;

            var buf = state.planeMoveSensivity * (1 - plane.control);

            plane.mesh.position.y += (targetY - plane.mesh.position.y) * deltaTime * (state.planeMoveSensivity - buf);
            plane.mesh.position.x += (targetX - plane.mesh.position.x) * deltaTime * (state.planeMoveSensivity - buf);

            buf = state.planeRotXSensivity * (1 - plane.control);
            plane.mesh.rotation.z = (targetY - plane.mesh.position.y) * deltaTime * (state.planeRotXSensivity - buf);
            buf = state.planeRotXSensivity * (1 - plane.control);
            plane.mesh.rotation.x = (plane.mesh.position.y - targetY) * deltaTime * (state.planeRotZSensivity - buf);
        },
        fall: function (mesh, deltaTime) {
            mesh.rotation.z += (-Math.PI / 2 - mesh.rotation.z) * .0002 * deltaTime;
            mesh.rotation.x += 0.0003 * deltaTime;
            mesh.position.y -= state.planeFallSpeed * deltaTime;
        },
        сollisionDetection: function (plane, enemies, coins, ambLight) {
            if (!plane || !plane.mesh) {
                console.warn("Plane mesh for collision detection is not define");
                return;
            }

            if (enemies) {
                for (var i = 0; i < enemies.enemiesInUse.length; i++) {
                    var e = enemies.enemiesInUse[i];
                    var diffPosition = plane.mesh.position.clone().sub(e.mesh.position.clone());
                    var d = diffPosition.length();
                    if (d < state.ennemyDistanceTolerance) {
                        particle.getHolder().spawn(
                            e.mesh.position.clone(),
                            15,
                            config.COLORS.red,
                            3,
                            particle.create);

                        enemies.pool.unshift(enemies.enemiesInUse.splice(i, 1)[0]);
                        enemies.mesh.remove(e.mesh);

                        state.planeCollisionSpeedX = 100 * diffPosition.x / d;
                        state.planeCollisionSpeedY = 100 * diffPosition.y / d;

                        if (ambLight) ambLight.intensity = 2;
                        state.fuel -= state.ennemyValue * (1 - plane.strength);
                        state.fuel = Math.max(0, state.fuel);
                        state.capturedEnnemyNumber++;
                        i--;
                    } else if (e.angle > Math.PI) {
                        enemies.pool.unshift(enemies.enemiesInUse.splice(i, 1)[0]);
                        enemies.mesh.remove(e.mesh);
                        i--;
                    }
                }
            }

            if (coins) {
                for (var i = 0; i < coins.coinsInUse.length; i++) {
                    var c = coins.coinsInUse[i];
                    var diffPosition = plane.mesh.position.clone().sub(c.mesh.position.clone());
                    var d = diffPosition.length();
                    if (d < state.coinDistanceTolerance) {
                        particle.getHolder().spawn(
                            c.mesh.position.clone(),
                            5,
                            config.COLORS.green,
                            .8,
                            particle.create);

                        coins.pool.unshift(coins.coinsInUse.splice(i, 1)[0]);
                        coins.mesh.remove(c.mesh);

                        state.fuel += state.coinValue;
                        if (state.fuel > state.maxFuel) state.fuel = state.maxFuel;
                        state.fuel = Math.max(0, state.fuel);
                        state.capturedCoinNumber++;
                        i--;
                    } else if (c.angle > Math.PI) {
                        coins.pool.unshift(coins.coinsInUse.splice(i, 1)[0]);
                        coins.mesh.remove(c.mesh);
                        i--;
                    }
                }
            }
        }
    }

    return obj;
});