define([
    'three',
    'src/state',
    'src/configuration',
    'src/utils/util',
    'text!shaders/terrain_fragment.glsl',
    'text!shaders/terrain_vertex.glsl'
], function (THREE, state, config, utils, fragShader, vertShader) {
    'use strict';

    var terrainProto = function () {
        var terra = {
            mesh: {},
            mountains: []
        }
        var geom = new THREE.CylinderGeometry(state.terrainRadius, state.terrainRadius, state.terrainLength, 40, 10);
        geom.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));

        geom.mergeVertices();
        var l = geom.vertices.length;

        for (var i = 0; i < l; i++) {
            var v = geom.vertices[i];
            terra.mountains.push({
                y: v.y,
                x: v.x,
                z: v.z,
                ang: Math.random() * Math.PI * 2,
                amp: state.mountainMinAmp + Math.random() * (state.mountainMaxAmp - state.mountainMinAmp),
                speed: 0
            });
        }

        var uniforms = {
            seaColor: {type: 'v3', value: [0.098, 0.204, 0.478]},
            seaAlt: {type: 'f', value: 585.0},
            bottomColor : { type: 'v3', value: [0.506, 0.588, 0.011]},
            bottomAlt: {type: 'f', value: 603.0},
            middleColor : { type: 'v3', value: [0.137, 0.098, 0.059]},
            topAlt: {type: 'f', value: 616.0},            
            topColor : { type: 'v3', value: [0.957, 0.937, 0.898]}            
        };

        var matterial = new THREE.ShaderMaterial({
            uniforms: THREE.UniformsUtils.merge([
                THREE.UniformsLib['lights'],
                THREE.UniformsLib['shadowmap'],
                THREE.UniformsLib['fog'],
                uniforms
            ]),
            vertexShader: utils.shaderParse(vertShader),
            fragmentShader: utils.shaderParse(fragShader),
            lights: true,
            fog: true,
            shading: THREE.FlatShading
        });
        terra.mesh = new THREE.Mesh(geom, matterial);
        terra.mesh.receiveShadow = true;

        return terra;
    }

    var obj = {
        create: function () {
            var terrain = terrainProto();
            terrain.mesh.position.y = -state.terrainRadius;
            return terrain;
        },
        animate: function (mesh, mountains) {
            var verts = mesh.geometry.vertices;
            var l = verts.length;

            for (var i = 0; i < l; i++) {
                var v = verts[i];
                var vprops = mountains[i];

                v.x = vprops.x + Math.cos(vprops.ang) * vprops.amp;
                v.y = vprops.y + Math.sin(vprops.ang) * vprops.amp;

                vprops.ang += vprops.speed;
            }

            mesh.geometry.verticesNeedUpdate = true;
            mesh.rotateZ(.005);
        }
    }

    return obj;
})