define([
    'three',
    'src/camera',
    'src/scene',
    'src/renderer',
    'src/light',
    'src/terrain',
    'src/sky',
    'src/plane',
    'src/enemy',
    'src/coins',
    'src/particles',
    'src/state',
    'src/utils/util',
    'src/configuration',
    'src/utils/mesh-loader',
    'src/utils/simple-defer',
    'src/utils/notificator',
    'src/utils/save-provider',
    'src/utils/detector',
    'src/utils/modal-provider',
    'src/utils/unit-manager'
], function (THREE, camera, scene, renderer, light, terrain, sky, plane, enemy, coin, particle, state, utils, configuration, loader, SimpleDeffer, notificator, saver, detector, modal, unitManager) {
    var mousePosition = { x: 0, y: 0 };
    var maxFuel = 0;
    var fuelBar = document.getElementById("fuelBar");
    var distanceField = document.getElementById('distValue');
    var levelField = document.getElementById("levelValue");
    var levelCircle = document.getElementById("levelCircleStroke");
    var replayMessage = document.getElementById("replayMessage");
    var pauseMessage = document.getElementById("pauseMessage");
    var pauseButton = document.getElementById("pauseButton");
    var bestScoreLabel = document.getElementById("bestScoreLabel");
    var moneyLabel = document.getElementById("moneyLabel");
    var marketButton = document.getElementById("marketButton");
    var hangarButton = document.getElementById("hangarButton");


    var directCamera = undefined;
    var ambLight = undefined;
    var deltaTime = 0;
    var newTime = new Date().getTime();
    var oldTime = new Date().getTime();
    var frameCounter = 0;
    var bestScore = undefined;
    var isMobilePortrait = false;

    function handleChangeOrientationEvent() {
        isMobilePortrait = detector.isMobilePortrait();
    }

    function handleMouseEvent(event) {
        var tx = -1 + (event.clientX / window.innerWidth) * 2;
        var ty = 1 - (event.clientY / window.innerHeight) * 2;

        mousePosition = { x: tx, y: ty };
    }

    function handleMouseUp(event) {
        if (state.status == configuration.GAME_STATE.waitingReplay) {
            state.reset();
            levelField.innerHTML = state.level;
            replayMessage.classList.toggle('no-display');
        }
    }

    function touchEventAdapter(event) {
        if (state.status != configuration.GAME_STATE.gaming) return;

        if (event.touches) {
            event.clientX = event.touches[0].clientX;
            event.clientY = event.touches[0].clientY;
        }
        handleMouseEvent(event);
    }

    function openNews(){
        if(saver.get(['lastSignInVer']).lastSignInVer === configuration.VER) return;
        var event = new Event("keypress");
        event.keyCode = 32;

        if (state.status == configuration.GAME_STATE.gaming) handleSpaceButtonPress(event);

        var context = {};

        modal.open("What's new in this update?", configuration.MODAL_TYPES.news, context,
            function () {
            },
            function () {
                saver.set({
                    lastSignInVer: configuration.VER
                });

                if (state.status == configuration.GAME_STATE.pause)
                    handleSpaceButtonPress(event);
            });
    }

    function openMarket(event) {
        event.stopPropagation();
        event.preventDefault();
        var event = new Event("keypress");
        event.keyCode = 32;

        var context = {
            money: saver.get(['money']).money,
            planes: configuration.MARKET_ITEMS.map(function (item) {
                return {
                    id: item.id,
                    maxSpeed: item.maxSpeed * 100,
                    fuelConsamption: item.fuelConsamption * 100,
                    control: item.control * 100,
                    strength: item.strength * 100,
                    name: item.name,
                    icon: item.icon,
                    price: item.price,
                    available: item.available,
                    notEnoughMoney: saver.get(['money']).money < item.price,
                    alreadyExist: state.planes.some(function(i){return i === item.id})
                };
            })
        };

        if (state.status == configuration.GAME_STATE.gaming) handleSpaceButtonPress(event);
        modal.open('Hello, in our market!', configuration.MODAL_TYPES.market, context,
            function () {
                var buttons = document.querySelectorAll('.buy-button');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].addEventListener('click', unitManager.buy, false);
                }
            },
            function () {
                moneyLabel.innerHTML = state.money;
                if (state.status == configuration.GAME_STATE.pause)
                    handleSpaceButtonPress(event);
            });
    }

    function openHangar(event) {
        event.stopPropagation();
        event.preventDefault();
        var event = new Event("keypress");
        event.keyCode = 32;

        if (state.status == configuration.GAME_STATE.gaming) handleSpaceButtonPress(event);

        var context = {
            planes: configuration.MARKET_ITEMS.filter(function (item) {
                return state.planes.some(function (i) { return i === item.id });
            }).map(function (item) {
                return {
                    id: item.id,
                    name: item.name,
                    icon: item.icon,
                    maxSpeed: item.maxSpeed * 100,
                    fuelConsamption: item.fuelConsamption * 100,
                    control: item.control * 100,
                    strength: item.strength * 100,
                    isSelected: item.id === state.selected
                }
            })
        };


        var oldSelectedPlane = state.selected;
        modal.open('Welcome in you hangar!', configuration.MODAL_TYPES.hangar, context,
        function(){
            var buttons = document.querySelectorAll('.pick-button');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].addEventListener('click', unitManager.pick, false);
                }
        },
        function () {
            if (state.status == configuration.GAME_STATE.pause)
                handleSpaceButtonPress(event);

            if(oldSelectedPlane !== state.selected)
                location.reload();
        });
    }

    function handleSpaceButtonPress(event) {
        event.stopPropagation();
        if (event.type === 'keypress' && event.keyCode !== 32) return;

        if (state.status == configuration.GAME_STATE.gaming) {
            togglePause();
            state.status = configuration.GAME_STATE.pause;
        } else if (state.status == configuration.GAME_STATE.pause) {
            togglePause();
            state.status = configuration.GAME_STATE.gaming;
        }
        event.preventDefault();
    }

    var sceneObjects = {
        earth: {},
        sky: {},
        plane: {},
        enemiesHolder: {},
        coinsHolder: {},
        particlesHolder: {}
    };

    function toggleReplay() {
        replayMessage.classList.toggle('no-display');
    }

    function togglePause() {
        pauseMessage.classList.toggle('no-display');
        pauseButton.classList.toggle('active');
    }

    function updateBestScore(forceRead, saveData) {
        bestScore = Math.floor(state.distance);
        if (forceRead && saveData) {
            bestScore = saveData.bestScore;
        }

        bestScoreLabel.innerText = bestScore;
    }

    function updateFuel() {
        state.fuel -= state.speed * deltaTime * state.ratioSpeedFuel * sceneObjects.plane.fuelConsumption;
        state.fuel = Math.max(0, state.fuel);
        fuelBar.style.right = (100 - (state.fuel / maxFuel) * 100) + "%";
        fuelBar.style.backgroundColor = (state.fuel / maxFuel < .4) ? "#" + configuration.COLORS.red.toString(16) : "#" + configuration.COLORS.green.toString(16);

        if (state.fuel / maxFuel < .25) {
            fuelBar.style.animationName = "blinking";
        } else {
            fuelBar.style.animationName = "none";
        }

        if (state.fuel < 1) {
            state.status = configuration.GAME_STATE.gameover;
            var tmpBestScore = Math.floor(state.distance);
            if (bestScore === tmpBestScore) {
                saver.set({
                    bestScore: tmpBestScore,
                    maxLevel: state.level,
                    money: state.money
                });
                notificator.showOk("Congratulation!", "Your new best score: ".concat(bestScore));
            } else {
                saver.set({
                    money: state.money
                });
            }
        }
    }

    function updateDistance() {
        if (state.status != configuration.GAME_STATE.gaming) return;
        state.distance += state.speed * deltaTime * state.ratioSpeedDistance;
        var disanceForView = Math.floor(state.distance);
        if (disanceForView > 10000) {
            disanceForView = Math.round(disanceForView / 100) / 10 + ' k';
        }
        distanceField.innerHTML = disanceForView;
        var d = 502 * (1 - ((state.distance - state.levelLastUpdate) % state.distanceForLevelUpdate) / state.distanceForLevelUpdate);
        levelCircle.setAttribute("stroke-dashoffset", d);

        if (state.distance > bestScore) {
            updateBestScore(false);
        }
    }

    function updatePlane() {
        state.planeSpeed = utils.normalize(mousePosition.x, -.5, .5, state.planeMinSpeed, state.planeMaxSpeed);
        state.planeCollisionDisplacementX += state.planeCollisionSpeedX;
        state.planeCollisionDisplacementY += state.planeCollisionSpeedY;

        state.planeCollisionSpeedX += (0 - state.planeCollisionSpeedX) * deltaTime * 0.03;
        state.planeCollisionDisplacementX += (0 - state.planeCollisionDisplacementX) * deltaTime * 0.01;
        state.planeCollisionSpeedY += (0 - state.planeCollisionSpeedY) * deltaTime * 0.03;
        state.planeCollisionDisplacementY += (0 - state.planeCollisionDisplacementY) * deltaTime * 0.01;

        plane.animate(sceneObjects.plane, mousePosition, deltaTime, isMobilePortrait);
    }

    var app = {
        init: function () {
            document.getElementById('threejs-container').addEventListener('mousemove', handleMouseEvent, false);
            document.getElementById('threejs-container').addEventListener('touchmove', touchEventAdapter, false);
            document.getElementById('threejs-container').addEventListener('mouseup', handleMouseUp, false);
            document.getElementById('threejs-container').addEventListener('touchend', handleMouseUp, false);
            window.addEventListener('orientationchange', handleChangeOrientationEvent, false);
            document.addEventListener('keypress', handleSpaceButtonPress, false);
            pauseButton.addEventListener('click', handleSpaceButtonPress, false);
            marketButton.addEventListener('click', openMarket, false);
            hangarButton.addEventListener('click', openHangar, false)

            state.save();
            var saveData = saver.get();
            updateBestScore(true, saveData);
            state.money = saveData.money;
            state.planes = saveData.planes;
            state.selected = saveData.selected;

            moneyLabel.innerHTML = state.money;
            handleChangeOrientationEvent();

            maxFuel = state.maxFuel;

            openNews();

            //Adding light
            var lights = light.create();
            lights.forEach((item) => {
                scene.add(item);
            });

            //Adding terrain
            sceneObjects.earth = terrain.create();

            //Adding sky
            sceneObjects.sky = sky.create();

            //Adding enemy pool
            var enemyPool = [];
            for (var i = 0; i < 10; i++) {
                var e = enemy.create();
                enemyPool.push(e);
            }
            sceneObjects.enemiesHolder = enemy.createHolder(enemyPool);

            //Adding coin pool
            var coinPool = [];
            for (var i = 0; i < 20; i++) {
                var c = coin.create();
                coinPool.push(c);
            }
            sceneObjects.coinsHolder = coin.createHolder(coinPool);

            //Adding particles pool;
            var particlePool = [];
            for (var i = 0; i < 10; i++) {
                var p = particle.create();
                particlePool.push(p);
            }
            sceneObjects.particlesHolder = particle.createHolder(particlePool);

            var modelLoadingPromises = [];
            modelLoadingPromises.push(new Promise(function (resolve, reject) {
                loader.load(state.selected, plane.construct, function (mesh) {
                    sceneObjects.plane = mesh;
                }).then(function (data) {
                    resolve(data);
                }, function (error) {
                    reject(error);
                });
            }));

            var defer = new SimpleDeffer();

            Promise.all(modelLoadingPromises).then(function () {
                for (var property in sceneObjects) {
                    if (sceneObjects.hasOwnProperty(property)) {
                        scene.add(sceneObjects[property].mesh);
                    }
                }

                renderer.create();
                console.log('MODELS ARE LOADED...')
                levelField.innerHTML = state.level;
                defer.resolve();
            }, function () {
                defer.reject();
            });

            return defer.promise;
        },
        loop: function () {
            newTime = new Date().getTime();
            deltaTime = newTime - oldTime;
            oldTime = newTime;
            var isAnimationEnabled = true;

            if (!directCamera) {
                directCamera = camera.create();
            }

            if (!ambLight) {
                ambLight = scene.children.filter(function (obj) {
                    return obj.type == 'AmbientLight';
                })[0];
            }

            if (state.status == configuration.GAME_STATE.gaming) {
                isAnimationEnabled = true;
                if (Math.floor(state.distance) % state.distanceForEnnemiesSpawn == 0 && Math.floor(state.distance) > state.ennemyLastSpawn) {
                    state.ennemyLastSpawn = Math.floor(state.distance);
                    sceneObjects.enemiesHolder.spawn(enemy.create);
                }

                if (Math.floor(state.distance) % state.distanceForCoinsSpawn == 0 && Math.floor(state.distance) > state.coinLastSpawn) {
                    state.coinLastSpawn = Math.floor(state.distance);
                    sceneObjects.coinsHolder.spawn(coin.create);
                }

                if (Math.floor(state.distance) % state.distanceForSpeedUpdate == 0 && Math.floor(state.distance) > state.speedLastUpdate) {
                    state.speedLastUpdate = Math.floor(state.distance);
                }

                if (Math.floor(state.distance - state.levelLastUpdate) === state.distanceForLevelUpdate && Math.floor(state.distance) > state.levelLastUpdate) {
                    state.levelLastUpdate = Math.floor(state.distance);

                    var m = 1 * state.level;
                    var coinRate = state.capturedCoinNumber / state.spawnedCoinNumber;
                    var ennemyRate = state.capturedEnnemyNumber / state.spawnedEnnemyNumber;
                    if (coinRate >= .9) m *= 1.25;
                    if (ennemyRate === 0) m *= 1.2;
                    if (ennemyRate <= .15) m *= 1.1;
                    if (ennemyRate >= .5) m *= .9;
                    state.money += Math.round(m);

                    state.spawnedCoinNumber = 0;
                    state.spawnedEnnemyNumber = 0;

                    state.level++;
                    levelField.innerHTML = Math.floor(state.level);
                    moneyLabel.innerHTML = state.money;

                    state.distanceForLevelUpdate = Math.floor(state.distanceForLevelUpdate * state.distanceForLevelUpdateRatio);
                }

                plane.сollisionDetection(sceneObjects.plane,
                    sceneObjects.enemiesHolder,
                    sceneObjects.coinsHolder,
                    ambLight
                );

                updateDistance();
                updateFuel();
                updatePlane();

                state.baseSpeed += (state.targetBaseSpeed - state.baseSpeed) * deltaTime * 0.02;
                state.speed = state.baseSpeed * state.planeSpeed * sceneObjects.plane.maxSpeed;
                camera.updateFieldOfView(directCamera, mousePosition, sceneObjects.plane.mesh.position.y, deltaTime);
            } else if (state.status == configuration.GAME_STATE.gameover) {
                isAnimationEnabled = true;
                state.speed *= .99;
                state.planeFallSpeed *= 1.05;
                plane.fall(sceneObjects.plane.mesh, deltaTime);

                if (sceneObjects.plane.mesh.position.y < -200) {
                    toggleReplay();
                    state.status = configuration.GAME_STATE.waitingReplay;
                }
            } else if (state.status == configuration.GAME_STATE.pause) {
                isAnimationEnabled = false;
                state.speed *= .0;
            } else {
                isAnimationEnabled = true;
            }

            if (isAnimationEnabled) {
                sceneObjects.enemiesHolder.animate(deltaTime);
                sceneObjects.coinsHolder.animate(deltaTime);
                terrain.animate(sceneObjects.earth.mesh, sceneObjects.earth.mountains);
                sky.animate(sceneObjects.sky.mesh, deltaTime);
            }
            if (ambLight && ambLight.intensity !== .5) {
                frameCounter++;
            } else {
                frameCounter = 0;
            }
            if (ambLight && frameCounter > 6) {
                ambLight.intensity -= .25;
            }

            renderer.get().render(scene, directCamera);
            window.requestAnimationFrame(app.loop);
        }
    }

    return app;
})