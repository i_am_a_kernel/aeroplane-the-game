define(['three', 'src/configuration', 'src/state'], function (THREE, config, state) {
    'use strict';


    var obj = {
        create: function () {
            var coin = {};
            var geom = new THREE.TetrahedronGeometry(5, 0);
            var mat = new THREE.MeshPhongMaterial({
                color: config.COLORS.green,
                shininess: 0,
                specular: 0xffffff,
                shading: THREE.FlatShading
            });
            coin.mesh = new THREE.Mesh(geom, mat);
            coin.mesh.castShadow = true;
            coin.angle = 0;
            coin.dist = 0;

            return coin;
        },
        createHolder: function (coins) {
            var holder = {};
            holder.mesh = new THREE.Object3D();
            holder.coinsInUse = [];
            holder.pool = coins;

            holder.spawn = function (createCoinCallback) {
                var count = 1 + Math.floor(Math.random() * 10);
                var d = state.terrainRadius + state.planeDefaultHeight + (-1 + Math.random() * (state.planeUpAmpHeight - 50)) - 50;
                var amplitude = 20 + Math.round(Math.random() * 10);

                var ab = Math.random() >= .7 ? -1 : 1;

                for (var i = 0; i < count; i++) {
                    var coin;
                    if (holder.pool.length) {
                        coin = holder.pool.pop();
                    } else {
                        coin = createCoinCallback();
                    }

                    coin.angle = -(i * 0.02);
                    coin.distance = d + Math.sin(i * .3) * (ab * amplitude);
                    coin.mesh.position.y = Math.sin(coin.angle) * coin.distance;
                    coin.mesh.position.x = Math.cos(coin.angle) * coin.distance;
                    holder.mesh.add(coin.mesh);
                    holder.coinsInUse.push(coin);
                    state.spawnedCoinNumber++;
                }
            };

            holder.animate = function (deltaTime) {
                for (var i = 0; i < holder.coinsInUse.length; i++) {
                    var c = holder.coinsInUse[i];
                    c.angle += state.speed * deltaTime * state.coinsSpeed;

                    if (c.angle > Math.PI * 2) c.angle -= Math.PI * 2;

                    c.mesh.position.y = -state.terrainRadius + Math.sin(c.angle) * c.distance;
                    c.mesh.position.x = Math.cos(c.angle) * c.distance;
                    c.mesh.rotation.z += Math.random() * .1;
                    c.mesh.rotation.y += Math.random() * .1;
                }
            };

            return holder;
        }
    }

    return obj;
});