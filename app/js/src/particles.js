define(['three', 'TweenMax', 'src/configuration', 'src/state'], function (THREE, TweenMax, config, state) {
    'use strict';

    var privateHolder = {};

    var obj = {
        create: function () {
            var particle = {};
            var geom = new THREE.TetrahedronGeometry(3, 0);
            var mat = new THREE.MeshPhongMaterial({
                color: 0x009999,
                shininess: 0,
                specular: 0xffffff,
                shading: THREE.FlatShading
            });
            particle.mesh = new THREE.Mesh(geom, mat);

            particle.explode = function (pos, color, scale) {
                if(!privateHolder) return;

                this.mesh.material.color = new THREE.Color(color);
                this.mesh.material.needsUpdate = true;
                this.mesh.scale.set(scale, scale, scale);

                var targetX = pos.x + (-1 + Math.random() * 2) * 50;
                var targetY = pos.y + (-1 + Math.random() * 2) * 50;
                var speed = .6 + Math.random() * .2;

                var _this = this;
                var _p = this.mesh.parent;
                TweenMax.to(this.mesh.rotation, speed, { x: Math.random() * 12, y: Math.random() * 12 });
                TweenMax.to(this.mesh.scale, speed, { x: .1, y: .1, z: .1 });
                TweenMax.to(this.mesh.position, speed, {
                    x: targetX, y: targetY, delay: Math.random() * .1, ease: Power2.easeOut, onComplete: function () {
                        if (_p) _p.remove(_this.mesh);
                        _this.mesh.scale.set(1, 1, 1);
                        privateHolder.pool.unshift(_this);
                    }
                });
            }

            return particle;
        },
        createHolder: function (enemies) {
            var holder = {};
            holder.mesh = new THREE.Object3D();
            holder.particlesInUse = [];
            holder.pool = enemies;
            holder.spawn = function (pos, density, color, scale, createParticleCallback) {
                var count = density;

                for (var i = 0; i < count; i++) {
                    var p;
                    if (holder.pool.length) {
                        p = holder.pool.pop();
                    } else {
                        p = createParticleCallback();
                    }

                    holder.mesh.add(p.mesh);
                    p.mesh.visible = true;

                    p.mesh.position.y = pos.y;
                    p.mesh.position.x = pos.x;

                    p.explode(pos, color, scale);
                }
            };

            privateHolder = holder;
            return holder;
        },
        getHolder: function () {
            return privateHolder;
        }
    }

    return obj;
});