define(['three', 'src/state', 'src/configuration'], function (THREE, state, config) {
    'use strict';

    var skyProto = function () {
        var sky = {};
        sky.mesh = new THREE.Object3D();
        sky.nClouds = config.COUNT_OF_CLOUDS;
        sky.clouds = [];
        var stepAngle = Math.PI * 2 / sky.nClouds;
        for (var i = 0; i < sky.nClouds; i++) {
            var c = cloudProto();
            sky.clouds.push(c);
            var a = stepAngle * i;
            var h = state.terrainRadius + 150 + Math.random() * 200;
            c.position.y = Math.sin(a) * h;
            c.position.x = Math.cos(a) * h;
            c.position.z = -300 - Math.random() * 500;
            c.rotation.z = a + Math.PI / 2;
            var s = 1 + Math.random() * 2;
            c.scale.set(s, s, s);
            sky.mesh.add(c);
        }

        sky.mesh.position.y = -state.terrainRadius;

        return sky;
    }

    var cloudProto = function () {
        var mesh = new THREE.Object3D();
        mesh.name = "cloud";
        var geom = new THREE.CubeGeometry(20, 20, 20);
        var mat = new THREE.MeshPhongMaterial({
            color: config.COLORS.white,
        });

        var nBlocs = 3 + Math.floor(Math.random() * 3);
        for (var i = 0; i < nBlocs; i++) {
            var m = new THREE.Mesh(geom.clone(), mat);
            m.position.x = i * 15;
            m.position.y = Math.random() * 10;
            m.position.z = Math.random() * 10;
            m.rotation.z = Math.random() * Math.PI * 2;
            m.rotation.y = Math.random() * Math.PI * 2;
            var s = .1 + Math.random() * .9;
            m.scale.set(s, s, s);
            m.castShadow = true;
            m.receiveShadow = true;
            mesh.add(m);
        }
        return mesh;
    }

    var rotateCloud = function(cloud){
        for(var i = 0; i < cloud.children.length; i++){
            var m = cloud.children[i];
            m.rotation.z += Math.random() * .005 * (i + 1);
            m.rotation.y += Math.random() * .002 * (i + 1);            
        }
    }


    var obj = {
        create: function () {
            var sky = skyProto();
            return sky;
        },
        animate: function (mesh, deltaTime) {
            for(var i = 0; i < mesh.children.length; i++){
                var cloud = mesh.children[i];
                rotateCloud(cloud);
            }
            mesh.rotation.z += state.speed * deltaTime;
        }
    }

    return obj;
});