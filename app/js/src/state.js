define(['src/configuration'], function (configuration) {
    'use strict';

    var savedState = undefined;

    var game = {
        speed: 0,
        initSpeed: .00045,
        baseSpeed: .0005,
        targetBaseSpeed: .00045,
        incrementSpeedByTime: .0000045,
        incrementSpeedByLevel: .000007,
        distanceForSpeedUpdate: 500,
        speedLastUpdate: 0,

        money: 0,
        planes: [],
        selected: 101,

        distance: 0,
        ratioSpeedDistance: 50,
        maxFuel: 100,
        fuel: 100,
        ratioSpeedFuel: 8,

        level: 1,
        levelLastUpdate: 0,
        distanceForLevelUpdate: 1000,
        distanceForLevelUpdateRatio: 1.1,

        planeDefaultHeight: 200,
        planeUpAmpHeight: 80,
        planeDownAmpHeight: 125,
        planeAmpWidth: 75,
        planeMoveSensivity: 0.005,
        planeRotXSensivity: 0.0008,
        planeRotZSensivity: 0.0004,
        planeFallSpeed: .001,
        planeMinSpeed: 1.0,
        planeMaxSpeed: 1.6,
        planeSpeed: 0,
        planeCollisionDisplacementX: 0,
        planeCollisionSpeedX: 0,

        planeCollisionDisplacementY: 0,
        planeCollisionSpeedY: 0,

        terrainRadius: 600,
        terrainLength: 800,
        mountainMinAmp: 5,
        mountainMaxAmp: 35,

        cameraFarPos: 10000,
        cameraNearPos: .1,
        cameraSensivity: 0.002,

        coinDistanceTolerance: 15,
        coinValue: 2,
        coinsSpeed: .5,
        coinLastSpawn: 0,
        distanceForCoinsSpawn: 100,
        spawnedCoinNumber: 0,
        capturedCoinNumber: 0,

        ennemyDistanceTolerance: 10,
        ennemyValue: 20,
        ennemiesSpeed: .6,
        ennemyLastSpawn: 0,
        distanceForEnnemiesSpawn: 50,
        spawnedEnnemyNumber: 0,
        capturedEnnemyNumber: 0,

        status: configuration.GAME_STATE.gaming,

        reset: function () {
            if (!savedState) {
                throw new Error('Saved state is undefined.');
            }
            for (var k in savedState) {
                if (savedState.hasOwnProperty(k) && typeof savedState[k] !== "function") {
                    game[k] = savedState[k];
                }
            }
        },
        save: function () {
            savedState = {};
            for (var k in game) {
                if (game.hasOwnProperty(k) && typeof game[k] !== "function") {
                    savedState[k] = game[k];
                }
            }
        }
    }

    return game;
});