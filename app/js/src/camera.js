define(['three', 'src/utils/container', 'src/state', 'src/utils/util'], function (THREE, container, state, util) {
    'use strict';

    var obj = {
        create: function () {
            var c = container.find();
            var aspectRatio = c.parentNode.offsetWidth / c.parentNode.offsetHeight;
            var fov = 50;
            var nearPlane = state.cameraNearPos;
            var farPlane = state.cameraFarPos;

            var camera = new THREE.PerspectiveCamera(
                fov,
                aspectRatio,
                nearPlane,
                farPlane
            );

            camera.position.x = 0;
            camera.position.y = state.planeDefaultHeight;
            camera.position.z = 200;
            var resizeEventHandler = function () {
                camera.aspect = c.parentNode.offsetWidth / c.parentNode.offsetHeight;
                camera.updateProjectionMatrix();
            }


            window.addEventListener('resize', resizeEventHandler, false);
            return camera;
        },
        updateFieldOfView: function (camera, mousePosition, planePositionY, deltaTime) {
            if (planePositionY < 0) planePositionY = state.planeDefaultHeight;
            var normalizedMousePosition = util.normalize(mousePosition.x, -1, 1, 40, 80);

            var scaleFactor = 0.15;
            var delta = Math.abs(normalizedMousePosition - camera.fov);

            scaleFactor = scaleFactor * delta / 40;

            camera.fov += (normalizedMousePosition - camera.fov) * scaleFactor;
            camera.position.y += (planePositionY - camera.position.y) * deltaTime * state.cameraSensivity;
            camera.updateProjectionMatrix();
        }
    }

    return obj;
});