define(['three', 'src/configuration', 'src/state'], function (THREE, config, state) {
    'use strict';

    var obj = {
        create: function () {
            var enemy = {};
            var geom = new THREE.TetrahedronGeometry(8, 2);
            var mat = new THREE.MeshPhongMaterial({
                color: config.COLORS.red,
                shininess: 0,
                specular: 0xffffff,
                shading: THREE.FlatShading
            });
            enemy.mesh = new THREE.Mesh(geom, mat);
            enemy.mesh.castShadow = true;
            enemy.angle = 0;
            enemy.dist = 0;

            return enemy;
        },
        createHolder: function (enemies) {
            var holder = {};
            holder.mesh = new THREE.Object3D();
            holder.enemiesInUse = [];
            holder.pool = enemies;
            holder.spawn = function (createEnemyCallback) {
                var count = state.level;

                for (var i = 0; i < count; i++) {
                    var e;
                    if (holder.pool.length) {
                        e = holder.pool.pop();
                    } else {
                        e = createEnemyCallback();
                    }

                    e.angle = - (i * 0.1);
                    e.distance = state.terrainRadius + state.planeDefaultHeight + (-1 + Math.random() * 2) * (state.planeUpAmpHeight - 20);
                    e.mesh.position.y = Math.sin(e.angle) * e.distance;
                    e.mesh.position.x = Math.cos(e.angle) * e.distance;

                    holder.mesh.add(e.mesh);
                    holder.enemiesInUse.push(e);
                    state.spawnedEnnemyNumber++;
                }
            };

            holder.animate = function (deltaTime) {
                for (var i = 0; i < holder.enemiesInUse.length; i++) {
                    var e = holder.enemiesInUse[i];
                    e.angle += state.speed * deltaTime * state.ennemiesSpeed;

                    if (e.angle > Math.PI * 2) e.angle -= Math.PI * 2;

                    e.mesh.position.y = -state.terrainRadius + Math.sin(e.angle) * e.distance;
                    e.mesh.position.x = Math.cos(e.angle) * e.distance;
                    e.mesh.rotation.z += Math.random() * .1;
                    e.mesh.rotation.y += Math.random() * .1;
                }
            };

            return holder;
        }
    }

    return obj;
});