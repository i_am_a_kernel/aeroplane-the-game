define(['three'], function(THREE) {
    'use strict';
    
    var scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0xeffaff, 100, 950);
    return scene;
});