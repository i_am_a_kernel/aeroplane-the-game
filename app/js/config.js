var require = {
	waitSeconds: 45,
	paths: {
		requirejs: 'vendor/requirejs/require',
		three: 'vendor/three.js/three',
		bowser: 'vendor/bowser/src/bowser',
		detector: 'src/utils/detector',
		meshLoader: 'src/utils/mesh-loader',
		TweenMax: "vendor/gsap/src/uncompressed/TweenMax",
		text: 'static/text',
		handlebars: 'vendor/handlebars/handlebars'
	},
	shim: {
		'three': { exports: 'THREE' }
	},
	packages: [
        {
            name: 'crypto-js',
            location: 'vendor/crypto-js',
            main: 'index'
        }
    ]
}
