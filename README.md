Agenda of readme:

[TOC]



Welcome to Aeroplane. The game!
===================

"Aeroplane. The game" is an tech demo of an three.js application, with functionality split into different Require.js modules.

In this game you is pilot of plane and should try to alive. Collect fuel points to continue flying and avoid big red blocks and terrain surface.

----------


Game design
-------------

###The Color Palette
![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_palette.png)

Let's define a color palette that will be used consistently throughout the project. For this project we choose the following colors:
```
{
	"red":"0xf25346",
	"white":"0xd8d0d1",
	"brown":"0x59332e",
	"pink":"0xF5986E",
	"brownDark":"0x23190f",
	"blue":"0x68c3c0",
}
```

###Scene

 1. **A scene**: consider this as the stage where every object needs to be added in order to be rendered
 2. **A camera**: in this case we will use a perspective camera, but it could also be an orthographic camera.
 3. **A renderer** that will display all the scene using WebGL.
 4. **One or more objects** to render, in our case, we will create a plane, a sea and a sky (a few clouds)
 5. **One or more lights**: there is also different types of lights available. In this project we will mainly use a hemisphere light for the atmosphere and a directional light for the shadows.

![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_three-components.png)

###Game objects
The clouds are a little bit more complex, as they are a number of cubes assembled randomly to form one shape.
![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_clouds.png)

Plane we can create in the same way thought Three.js primitives
![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_plane-of-cubes.png)

or using **the best way** create 3D models in Blender3D and export meshes to json with [Blender to Three.js exporter](https://github.com/mrdoob/three.js/tree/master/utils/exporters/blender)

###Pilot
Adding a pilot to our airplane is just as easy as adding a couple of boxes.
But we don't just want any pilot, we want a cool pilot with windblown, animated hair! It seems like a complicated endeavor, but since we are working on a low-poly scene it becomes a much easier task.
![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_hair.png)

or use ready-to-use pilot 3D model like plane.

###Mountains
You have probably noticed that the mountains doesn�t really look like a mountains , but more like a surface that was flattened by a large roller.
It needs some relief. This can be done by combining two techniques we have used earlier:

 1. Manipulating the vertices of a geometry.
 2. Applying a cyclic movement to each vertex.
 
To make mountains we will rotate each vertex of the cylinder around its initial position, by giving it a random speed rotation, and a random distance (radius of the rotation). Sorry, but you�ll also need to use some trigonometry here!

![enter image description here](https://codropspz-tympanus.netdna-ssl.com/codrops/wp-content/uploads/2016/04/Animated3DScene_sea_manipulation.png)

###Smoother Flight
When the plane changes its altitude it would be nice if it changed its position and orientation more smoothly. In this final bit of the tutorial we will implement exactly that.
An easy way to do that would be to make it move to a target by adding a fraction of the distance that separates it from this target in every frame.
To be more realistic, the rotation of the plane could also change according to the direction of the movement. If the plane goes up very fast, it should quickly rotate counterclockwise. If the planes moves down slowly, it should rotate slowly in a clockwise direction.
To achieve exactly that, we can simply assign a proportional rotation value to the remaining distance between the target and the position of the plane.
By changing the fraction values, we can make the plane respond faster or slower to the mouse movement. 

----------


Development
-------------------

To start development follow next steps:

 1. Clone git repository
```
git clone git@bitbucket.org:i_am_a_kernel/aeroplane-the-game.git
cd aeroplane-the-game
```
> **Note:**

> To get access you should send request on [**arthur.osmokiesku@gmail.com**](mailto:arthur.osmokiesku@gmail.com)

2. Install **npm**, **bower** and **gulp** globaly
3. Install all dependencies with custom command (also it will globaly install firebase cli)
```
npm run setup
```
4. Execute next command to build project
```
npm run build
```
5. And for build and start locally with file watcher
```
npm run dev
```

Deployment
-------------

This project fully ready to deploy on **Google Firebase**
Call next command in terminal to deploy
```
firebase login //(optional if not logged in)
firebase deploy
```

If you want to get current deployed version just open in browser <APP_NAME>.firebaseapp.com/ver.  
Although, If you have permission you can open console on [Google Firebase](https://console.firebase.google.com/u/0/project/aeroplane-32465/overview)

Browser compatibility
-------------

###Desktop
OS		| Browser | Version | Status
-------- | --- | --- | ---- |
Windows | IE | **All** | Unsuported|
Windows | Edge| 12+ | OK|
Windows| Chrome | 47+| OK|
Windows| FF| 51+| OK|
Windows| FF| <51| N/A|
OSX| Safari| 8+| OK|
OSX| Safari| <8| Unsuported|
Linux| **All** | **All**| N/A|

###Mobile
OS|Browser|Version|Staus
-----|------|------|------|
iOS|Safari| 10+ | OK|
iOS|Safari| <10| N/A/|
Android| Native| 4.4+| OK|
Android| Chrome| 51+| OK|
Android| Chrome| <51| N/A|
Android| FF| 52+| OK|
Android| FF| <52| Unsuported|
WindowsPhone| **All**| **All**| Unsuported|

3D modeling
-------------
For notice folow next steps to prepare 3d model for three.js engine.

 1. Create model in 3DS Max 
 2. Save it in *.max format (put in repository)
 3. Export in *.obj 
 4. Open*.obj file in blender 3D and adjust by asix.
 5. Export in THREE.js as geometry with enabled Face materials 
 6. Obtained *.json  save in model's folder with the same name as  *.max file. 
 7. Delete *.obj и *.mth files.
 
Units game balance
-------------
[You can view and comment balance here](https://docs.google.com/spreadsheets/d/1zBO0EIwCy9mVlyukRErJl1PGTE8vV5S-Bqmi_yDdi5M/edit?usp=sharing)

Roadmap
-------------
 - Beta **[JULY 31]**
	 - Enemies
	 - Fuel coins	 
	 - Particle system
	 - Model loading
	 - 3D model of paper plane
	 - Loading screen
	 - New mechanic of plane control
 - RC **[SEPTEMBER 3]**
	 - Distance measurement
	 - Deviding by levels evels
	 - Land painting which is based on height.
	 - Domain name
	 - Best score
	 - Pilot avatar
 - v1.0 **[SEPTEMBER 17]**
 - v1.1 **[OCTOBER 15]**
	 - 3 new planes
	 - Inner curency (Ace points)
	 - Plane store
	 - Hangar for  planes
 - v1.2 **[NOVEMBER 12]**
	 - 3 new planes
	 - Authentication with Google
	 - Best score synchronization
	 - Ads
 - v1.3 **[DECEMBER 24]**
	 - 2 new planes
	 - Global raiting
	 - Achievements
 - v1.4 **[JANUARY13]**
	 - 2 new planes
	 - New type of enemy

### Enjoy!!!